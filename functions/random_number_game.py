import random


def play(lifes):
    random_number = random.randint(1, 100)
    chosen_number = None

    while random_number != chosen_number:
        chosen_number = int(input("Enter a number between 1 and 100: "))

        if random_number < chosen_number:
            print("Choose a smaller number: ")
            lifes -= 1
        elif random_number > chosen_number:
            print("Choose a larger number")
            lifes -= 1

        if lifes == 0:
            print('GAME OVER')
            print(f'The number was {random_number}')
            break

        print(f'{lifes} left')

    if chosen_number == random_number:
        print("YOU WON")


def main():
    while True:
        menu = """
        GUESS THE RANDOM NUMBER
        1 - Easy Level
        2 - Mid Level
        3 - Hard Level
        4 - Exit
        Enter a option: """

        option = input(menu)

        if option == '1':
            play(10)
        elif option == '2':
            play(7)
        elif option == '3':
            play(5)
        elif option == '4':
            print('Exit Game')
            break
        else:
            print("Enter a valid option")


if __name__ == '__main__':
    main()

