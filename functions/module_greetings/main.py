import greetings

message = greetings.hello("c3rberus  ")
print(message)

messages = greetings.hellos("GG", "GG2", "GG3")
for message in messages.values():
    print(message)