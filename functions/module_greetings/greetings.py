import random
from typing import Dict

def random_format() -> str:
    formats = [
        "Hello, {}! Welcome!",
        "Is great see you, {}",
        "Greetings {}! Nice to meet you"
    ]
    # Choice select a random item from the list
    random_message = random.choice(formats)
    return random_message


def hello(name: str) -> str:
    if name == '':
        return f"Error: Name cannot be an empty string."

    message = random_format().format(name)
    return message


def hellos(*names: str) -> Dict[str, str]:
    messages = {}

    for name in names:
        message_h = hello(name)
        messages[name] = message_h

    return messages
