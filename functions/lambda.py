add = lambda a, b: a + b
double = lambda n: n * 2
par = lambda n: n % 2 == 0
impar = lambda n: n % 2 != 0
reverse = lambda text: text[::-1]

print(add(5, 5))
print(double(100))
print(par(5))
print(impar(5))
print(reverse("hola"))
