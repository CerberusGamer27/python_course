# Function to check if word is palindrome

def palindrome_word(word):
    # Clear white spaces in the word
    word = word.replace(' ', '')
    word = word.lower()
    inverted_word = word[::-1]
    if word == inverted_word:
        return True
    else:
        return False

# This is the main function of the app


def main():
    input_word = input("Write the word: ")
    is_palindrome = palindrome_word(input_word)
    print("Is palindrome") if is_palindrome else print("Not is palindrome")


# Entry point of the app
if __name__ == '__main__':
    main()
