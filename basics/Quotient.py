print("Calcular cociente y residuo")

# Data Input
num1 = input("Ingrese el primer numero: ")
num2 = input("Ingrese el segundo numero: ")

# Casting
num1 = int(num1)
num2 = int(num2)

# Double // calculate the quotient
quotient = num1 // num2
remainder = num1 % num2

print("Quotient: "+str(quotient))
print("Remainder: "+str(remainder))
