print("------------------------------")
print("-----Calculate sell Price-----")

# Data Input
sell_price = input("Enter the selling price: ")

# Casting
sell_price = float(sell_price)

#
IGV = 0.18
price_with_igv = sell_price * IGV
final_price = price_with_igv + sell_price

# Print Information
print("-----  TICKET   -----")
print("SELL VALUE: ", sell_price)
print("IGV: ", price_with_igv)
print("FINAL PRICE: ", final_price)