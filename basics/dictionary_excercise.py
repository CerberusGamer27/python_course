# List for students name
students = []

# Main Menu
while True:
    menu = """
    1. Register Student
    2. Show Record
    3. Quit
    4. Enter a Option => 
    """
    option = input(menu)
    if option == '1':
        # Registering the student data
        name = input("Student Name: ")
        age = int(input("Student Age: "))
        course = input("Student Course: ")

        # Dictionary
        student = {"name": name, "age": age, "course": course}
        students.append(student)
        print("Student Registered Successfully!")
    elif option == '2':
        if students:
            print("Students Records \n")
            for student in students:
                print(f"Student: {student['name']} - Age: {student['age']} - Course: {student['course']}\n")
        else:
            print("You have not registered yet!")
    elif option == '3':
        print('Thank you for registering')
        break
    else:
        print("Invalid Option")

