# Definitions
import random

initial_values = (1, 2, 3, 4, 5, 6, 7, 8, 9)
odds = []
evens = []

for val in initial_values:
    random_number = random.randint(1, 100)
    generated_number = random_number * val
    print(f"{val} x {random_number} = {generated_number}")
    evens.append(generated_number) if generated_number % 2 == 0 else odds.append(generated_number)

print("Odds: ", odds)
print("Evens: ", evens)
