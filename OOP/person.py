
class Person:

    # __init__ is the constructor of class
    def __init__(self, name, age):
        self.name = name
        self.age = age


    def show_data(self):
        print(f'Name: {self.name}')
        print(f'Age: {self.age}')

    def __str__(self):
        return f'Name: {self.name}, Age: {self.age}'


