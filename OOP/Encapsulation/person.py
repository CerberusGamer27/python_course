class Person:
    __name = None
    __age = None

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __private_method(self):
        print('Soy un metodo privado')

    def __str__(self):
        return f'Name: {self.name}, Age: {self.age}'
