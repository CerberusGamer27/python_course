from figures import *


def main():
    while True:
        menu = """
        AREA and PERIMETER OF GEOMETRIC FIGURES
        
        1 - Rectangle
        2 - Circle
        3 - Exit
        Enter an Option: 
        """
        option = input(menu)

        if option == '1':
            length = float(input('Enter a length: '))
            width = float(input('Enter a Width: '))
            r = Rectangle(length, width)
            test_figure(r)
        elif option == '2':
            radio = float(input('Enter a radio: '))
            c = Circle(radio)
            test_figure(c)
        elif option == '3':
            print('Exit')
            break
        else:
            print('Invalid Option')


if __name__ == '__main__':
    main()
