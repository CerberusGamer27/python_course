import math


class Figure(object):
    def __init__(self, name):
        self.name = name

    def area(self):
        pass

    def perimeter(self):
        pass


class Rectangle(Figure):

    def __init__(self, length, width):
        self.name = __class__.__name__
        self.length = length
        self.width = width

    def area(self):
        return self.length * self.width

    def perimeter(self):
        return (2 * self.length) + (2 * self.width)

    def __str__(self):
        return f'{self.name}[length:{self.length} width:{self.width}]'


class Circle(Figure):

    def __init__(self, radio):
        self.name = __class__.__name__
        self.radio = radio

    def area(self):
        return math.pi * self.radio * self.radio

    def perimeter(self):
        return 2 * math.pi * self.radio

    def __str__(self):
        return f'{self.name}[radio: {self.radio}]'


def test_figure(figure):
    print(figure)
    print('Area: ', figure.area())
    print('Perimeter:', figure.perimeter())

